const API_ROOT = "http://io.dejuandroandlydia.com"
const Confirmation_Path = "confirmation/postdatatamu"

export default {
    sendConfirmation: async (params_data, update) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                "nama": params_data.Nama,
                "nohp": params_data.NoHp,
                "jumlah_orang": params_data.Jumlah_Orang,
                "kenalan": params_data.Kenalan,
                "kehadiran":params_data.Kehadiran,
                "pesan":params_data.Pesan
            })
        };
        const SendData = await fetch(`${API_ROOT}${Confirmation_Path}`, requestOptions)
        const response = await SendData.json()
        return (response)
    }
}