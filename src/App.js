// import './App.css';
import * as React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { render } from 'react-dom';
import WeddingConfirmation from './WeddingConfirmation/WeddingConfirmation';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
  Redirect,
} from "react-router-dom";


function Invitation() {
  let history = useHistory();


  function handleClick() {
    history.push("/confirmation");
  }

  return (
    <div>
      <h2>
        Wedding Invitation is Coming Soon !
      </h2>
      <a href={''} onClick={()=>{handleClick()}}>
        Klik disini untuk ke form konfirmasi.
      </a>
    </div>
  )

}


function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Invitation />
        </Route>
        <Route path="/confirmation">
          <WeddingConfirmation />
        </Route>
      </Switch>
    </Router>

    
  )

}

export default App;
